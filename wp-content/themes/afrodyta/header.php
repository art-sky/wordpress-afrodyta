<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Afrodyta</title>
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/reset.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/style_2.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/hover.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/animate.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/mediaquaries.css">
</head>
<body>
    <header class="before">
        <div class="container">
            <div class="heading clearfix">
                <img src="<?php bloginfo('template_directory') ?>/img/logo.png" alt="logo" class="logo">
                <nav id="menu" class="menu1">
                    <ul class="menu">
                    <?php 
                    $args = array(
                        'theme_location'  => '',
                    	'menu'            => '', 
                    	'container'       => 'ul', 
                    	'container_class' => 'menu', 
                    	'container_id'    => '',
                    	'menu_class'      => 'menu', 
                    	'menu_id'         => '',
                    	'echo'            => true,
                    	'fallback_cb'     => 'wp_page_menu',
                    	'before'          => '',
                    	'after'           => '',
                    	'link_before'     => '',
                    	'link_after'      => '',
                    	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    	'depth'           => 0,
                    	'walker'          => ''
                    );
                    wp_nav_menu($args); ?>
                        <!-- <li><a href="#news">ABOUT</a>
                        </li>
                        <li><a href="#art3">GALLERY</a>
                        </li>
                        <li><a href="#categories">BLOG</a>
                        </li>
                        <li><a href="#art4">CONTACT</a>
                        </li> -->
                    </ul>
                </nav>
            </div>
        </div>
    </header>
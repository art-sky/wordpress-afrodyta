<footer id="footer">
        <div class="container">
           <nav class="menu3">
                <ul class="menu33">
                    <li><a href="#">ABOUT</a>
                    </li>
                    <li><a href="#">TERMS AND CONDITioNS</a>
                    </li>
                    <li><a href="#">SYMU FREEBIES</a>
                    </li>
                    <li><a href="#">CONTACT US</a>
                    </li>
                </ul>
            </nav>
            <p class="p-text">This tempalte is free to use for personal or commercial projects, attribution is appreciated but not required. <br>
            If you have any additional questions that may not have been answered above, pleasdon't hesitate to contact us at contact@symu.com</p>
            <div class="soc">
                <a href="https://www.facebook.com/"><img src="<?php bloginfo('template_directory') ?>/img/facebook.png" alt="facebook"></a>
                <a href="https://twitter.com"><img src="<?php bloginfo('template_directory') ?>/img/twitter.png" alt="twitter"></a>
                <a href="https://www.youtube.com"><img src="<?php bloginfo('template_directory') ?>/img/utube.png" alt="youtube"></a>
                <a href="#"><img src="<?php bloginfo('template_directory') ?>/img/xz.png" alt="xz"></a>
                <a href="https://www.instagram.com/"><img src="<?php bloginfo('template_directory') ?>/img/inst.png" alt="instagram"></a>
            </div>
        </div>
    </footer>
</body>
<script src="<?php bloginfo('template_directory') ?>/js/jquery-3.3.1.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/common.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/parallax.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/script.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/jquery.waypoints.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/animate-css.min.js"></script>
</html>
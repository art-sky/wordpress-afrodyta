<?php get_header(); ?>

    <section id="banner" class="banner">
        <div class="container">
            <div class="parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory') ?>/img/banner4.png">

            <h1><?php bloginfo('name') ?></h1>
            <div class="achor-text">
                <a href="#footer"><img src="<?php bloginfo('template_directory') ?>/img/circle14.png" alt="">ENJOY THE PLEASURE</a>
            </div>
            </div>
        </div>
    </section>
    <section id="art1" class="art1">
        <div class="container">
            <h3>07/08</h3>
            <img src="<?php bloginfo('template_directory') ?>/img/2.png" alt="2">
            <h1>Hinc nobis<br>duo ut</h1>
            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur</p><br><br>
            <a class="a-text" href="#">read more...</a>
        </div>
    </section>
    <section id="art2" class="art2">
        <div class="container">
                <img src="<?php bloginfo('template_directory') ?>/img/3.png" width="542" height="587" alt="3" id="img3">
                <img src="<?php bloginfo('template_directory') ?>/img/back1.jpg" width="1001" height="162" alt="back1" id="back1">
                <a class="a-text" href="#">read more...</a>
                <h3>28/07</h3>
                <div class="art2-text">
                    <h2>Ea eum cibot</h2>
                    <h1>graece</h1>
                </div>
                <p>Duis aute irure dolor in reprehenderit involuptatevelit esse cillum dolore eu fugiat nullapariatur.Excepteur sint occaecat cupidatat non proident, suntin culpa qui officia deserunt mollitanim id estlaborum. Sed ut perspiciatis unde omnisiste natuserror sit voluptatem</p>
        </div>
    </section>
    <section id="art3">
        <div class="container">
            <div class="art3">
                <h3>15/07</h3>
                <img src="<?php bloginfo('template_directory') ?>/img/4.png" alt="4">
                <img src="<?php bloginfo('template_directory') ?>/img/back2.jpg" width="440" height="540" alt="back2" id="back2">
                <h1>EOS NO<br>LIBER<br>IUDICABIT.</h1>
            </div>
        </div>
    </section>
    <section id="news" class="news">
        <div class="container">
            <img src="<?php bloginfo('template_directory') ?>/img/5.png" alt="5" id="img_news">
            <h1>SIGN UP FOR NEWSLETTER</h1>
            <a class="butt" href="#">sign up</a>
        </div>
    </section>
    <section id="info" class="info">
        <div class="container">
            <nav class="menu2">
                <ul class="menu22">
                    <li><a href="#">vitae</a>
                    </li>
                    <li><a href="#">audiam</a>
                    </li>
                    <li><a href="#">dican</a>
                    </li>
                    <li><a href="#">inermis</a>
                    </li>
                </ul>
            </nav>
            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,<br>consectetur, adipisci velit, sedquia non numquam eius modi temporaincidunt ut labore et doloremagnam aliquam</p>
        </div>
    </section>
    <section id="categories">
        <div class="container">
            <div class="categories">
                <div class="categ">
                    <figure class="effect-ming">
                        <img class="img-responsive" src="<?php bloginfo('template_directory') ?>/img/6.png" width="360" height="468" alt=""/> 
                        <h1>MEI</h1>
                        <figcaption>
                           <span>
                               <a class="html-popup" href="#"><img class="img-responsive" src="<?php bloginfo('template_directory') ?>/img/plus.png" alt=""></a>
                           </span>
                        </figcaption>
                    </figure>
                </div>
                <div class="categ">
                    <figure class="effect-ming">
                        <img class="img-responsive" src="<?php bloginfo('template_directory') ?>/img/7.png" width="360" height="468" alt=""/> 
                        <h1>CIBO</h1>
                        <figcaption>
                           <span>
                               <a class="html-popup" href="#"><img class="img-responsive" src="<?php bloginfo('template_directory') ?>/img/plus.png" alt=""></a>
                           </span>
                        </figcaption>
                    </figure>
                </div>
                <div class="categ">
                    <figure class="effect-ming">
                        <img class="img-responsive" src="<?php bloginfo('template_directory') ?>/img/8.png" width="360" height="468" alt=""/> 
                        <h1>eum</h1>
                        <figcaption>
                           <span>
                               <a class="html-popup" href="#"><img class="img-responsive" src="<?php bloginfo('template_directory') ?>/img/plus.png" alt=""></a>
                           </span>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <section id="art4" class="art4">
        <div class="container">
            <img src="<?php bloginfo('template_directory') ?>/img/9.png" width="542" height="587" alt="9" id="img9">
            <img src="<?php bloginfo('template_directory') ?>/img/back3.jpg" width="1001" height="436" alt="back3" id="back3">
            <a class="a-text" href="#">read more...</a>
            <div class="art4-text">
                <h2>Pri att mediocrem</h2>
                <h1>corrumpit</h1>
            </div>
            <p>Accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
                <br><br>
            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipiscivelit,sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquamquaeratvoluptatem.</p>
        </div>
    </section>


<?php get_footer(); ?>
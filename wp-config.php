<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'Afrodyta');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?x-XGI$C38Y(Mrr[wGgJ]E|MU:N>9XP#=n&0Fcb,|+-$S$ZL}1UPr5u6Ue(c-P>]');
define('SECURE_AUTH_KEY',  'b%Wc:l(p3xUan|TtvuO3J=*lKd+vR~]=Azke1Ik`z8EtdC{#b}x?V.euD[+rM`{R');
define('LOGGED_IN_KEY',    'Up $fo@Ca-~5J*1w{I!$qbNSg]v!*CCL  ok5B4k+& nxWx[uT#etqS)COI27?Tm');
define('NONCE_KEY',        ']JKMsgmD9yy,et=9:aTEfo4<!nW14&M;wz+]9E}>Mn<)ubv*DR2ul;uXbxpg73N;');
define('AUTH_SALT',        '1@4qGaH|oLsvYMz9?SRnn-Z`XYJxm}_!PHV;z{mPjfx`L[zz&|GLG6p2r*pG{A_S');
define('SECURE_AUTH_SALT', 'mPt1n+;r*e9CFfn(1IT0KUg47&7 t5]QINQ/o=EE,LVvo/&(H8)rjw|jk!V6@p`4');
define('LOGGED_IN_SALT',   'r|T:MXOGTp91/e`CM9bA7g}tLtp3)7<%Su5jXNAE@3}xTq!R%|>8$JPT.X:I,F[z');
define('NONCE_SALT',       ';szccbbTLW/_8~d2 w`kW46(Z93Z:WdD(6C h8btPtd}]E:6{8[dW`2>eAU<c?D ');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
